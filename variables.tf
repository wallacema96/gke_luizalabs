variable "project_id" {
  type    = string
  default = "luizalabs-345213"
}

variable "region" {
  type = string
  default = "us-central1"
}

variable "gke_num_nodes" {
  default     = 1
  description = "numero de nodes para o cluster"
}