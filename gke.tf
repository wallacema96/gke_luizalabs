provider "google" {
  credentials = "${file("luizalabs-345213-43e213472bb7.json")}" //git ignore
  project     = "luizalabs-345213"
  region      = "us-central1"
}

# Cluster GKE
resource "google_container_cluster" "wallacema_cluster" {
  name     = "${var.project_id}-gke"
  location = "us-central1"
  remove_default_node_pool = true
  initial_node_count       = 1
}

resource "google_container_node_pool" "primary_preemptible_nodes" {
  name       = "${google_container_cluster.wallacema_cluster.name}-node-pool"
  location   = "us-central1"
  cluster    = google_container_cluster.wallacema_cluster.name
  node_count = var.gke_num_nodes

  node_config {
    machine_type = "e2-standard-2"
    oauth_scopes = [
      "https://www.googleapis.com/auth/logging.write",
      "https://www.googleapis.com/auth/monitoring",
    ]
    labels = {
      env = sensitive(var.project_id)
    }
    tags         = ["gke-node", sensitive("${var.project_id}-gke")]
    metadata = {
    disable-legacy-endpoints = "true"
    }
  }
}

resource "google_compute_firewall" "my-firewall" {
  name = "all"
  network = "default"
  allow {
    protocol  = "all"
  }
 source_ranges = ["0.0.0.0/0"]
}